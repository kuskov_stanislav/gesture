import cv2
import numpy as np
import math
import csv

cap = cv2.VideoCapture(0)
while(cap.isOpened()):

    retr, img = cap.read()
    cv2.rectangle(img,(300,300),(100,100),(0,255,255),0)
    crop_img = img[100:300, 100:300]
    grey = cv2.cvtColor(crop_img, cv2.COLOR_BGR2GRAY)
    value = (35, 35)
    blurred = cv2.GaussianBlur(grey, value, 0)
    #     hsv = cv2.cvtColor(crop_img, cv2.COLOR_BGR2HSV)
    #
    #     blurred = cv2.blur(hsv, (3, 3))
    #
    #     lower_blue = np.array([0, 27, 46])  # camera web best
    #     upper_blue = np.array([34, 155, 237])

    _, thresh1 = cv2.threshold(blurred, 127, 255,
                                cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    #     thresh1 = cv2.inRange(blurred, lower_blue, upper_blue)
    #     thresh1 = cv2.erode(thresh1, None, iterations=2)  # delete other white pixels
    #     thresh1 = cv2.dilate(thresh1, None, iterations=6)  # not change ROI blob size in prev function (erode)

    cv2.imshow('Thresholded', thresh1)

    (version) = cv2.__version__.split('.')

    # if version is '3':
    #     image, contours, hierarchy = cv2.findContours(thresh1.copy(), \
    #            cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    # elif version is '2':
    contours, hierarchy = cv2.findContours(thresh1.copy(),cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)

    cnt = max(contours, key = lambda x: cv2.contourArea(x))

    x,y,w,h = cv2.boundingRect(cnt)
    cv2.rectangle(crop_img,(x,y),(x+w,y+h),(0,255,255),0)
    hull = cv2.convexHull(cnt)
    drawing = np.zeros(crop_img.shape,np.uint8)
    cv2.drawContours(drawing,[cnt],0,(0,255,0),0)
    cv2.drawContours(drawing,[hull],0,(0,0,255),0)
    hull = cv2.convexHull(cnt,returnPoints = False)
    defects = cv2.convexityDefects(cnt,hull)
    count_defects = 0
    cv2.drawContours(thresh1, contours, -1, (0,255,0), 3)

    with open('counturs.csv', 'a') as csv_file:
        csv_writer = csv.writer(csv_file)
        for item in contours:
            csv_writer.writerow([item])

    for i in range(defects.shape[0]):
        s,e,f,d = defects[i,0]
        start = tuple(cnt[s][0])
        end = tuple(cnt[e][0])
        far = tuple(cnt[f][0])
        a = math.sqrt((end[0] - start[0])**2 + (end[1] - start[1])**2)
        b = math.sqrt((far[0] - start[0])**2 + (far[1] - start[1])**2)
        c = math.sqrt((end[0] - far[0])**2 + (end[1] - far[1])**2)
        angle = math.acos((b**2 + c**2 - a**2)/(2*b*c)) * 57
        if angle <= 90:
            count_defects += 1
            cv2.circle(crop_img,far,1,[0,0,255],-1)
        #dist = cv2.pointPolygonTest(cnt,far,True)
        cv2.line(crop_img,start,end,[0,255,0],2)
        #cv2.circle(crop_img,far,5,[0,0,255],-1)

    cv2.imshow('Gesture', img)
    all_img = np.hstack((drawing, crop_img))
    cv2.imshow('Contours', all_img)
    k = cv2.waitKey(10)
    if k == 27:
        break
